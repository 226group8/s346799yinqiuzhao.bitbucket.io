//window.onload execute code after html loaded 
window.onload = function () {
    //tag input data 
    var flag_username = false;
    var flag_passwd1 = false;
    var flag_passwd2 = false;
    var flag_email = false;
    var flag_address = false;
    var flag_mobile = false;
    var flag_birthday = false;
    //id gets the html node object 
    //input
    var uesrname = document.getElementById('username');
    var passwd1 = document.getElementById('passwd1');
    var passwd2 = document.getElementById('passwd2');
    var email = document.getElementById('email');
    var address = document.getElementById('address');
    var mobile = document.getElementById('mobile');
    var birthday = document.getElementById('birthday');
    //console.log(birthday);
    //button
    var button1 = document.getElementById('btn1');
    //span
    var span1 = document.getElementById("span1");
    var span2 = document.getElementById("span2");
    var span3 = document.getElementById("span3");
    var span4 = document.getElementById("span4");
    var span5 = document.getElementById("span5");
    var span6 = document.getElementById("span6");
    var span7 = document.getElementById("span7");

    //click button1 event 
    button1.onclick = function () {
        if (flag_username && flag_passwd1 && flag_passwd2 && flag_email && flag_address && flag_mobile && flag_birthday) {
            //General submission and database comparison 
            alert("✓ Congratulation registration success !");
            window.location.href = "menu.html";
        } else {
            alert("✖ There are entries that do not meet the registration rules !");
        }
    }

    //username
    //lose focus
    uesrname.onblur = function () {
        //Match with regular expressions 
        var reg = /^[0-9A-Za-z]{6,14}$/;
        //The result of matching is true or false 
        if (username.value == "") {
            span1.style.color = 'rgba(253, 40, 69, 1)';
            span1.innerText = '✖ Username can not be blank ';
            flag_passwd2 = false;
        } else if (reg.test(username.value)) {
            //Set properties in a node 
            span1.style.color = 'green';
            span1.innerText = '✓ Available';
            flag_username = true;
        } else {
            span1.style.color = 'rgba(253, 40, 69, 1)';
            span1.innerText = '✖ Unavailable (6~14 alphanumeric characters) ';
            flag_username = false;
        }
    }
    //When the focus is obtained, it is judged whether the input data format is legal or not. If it is illegal, it needs to be input again. 
    username.onfocus = function () {
        if (!flag_username) {
            username.value = "";
            span1.innerText = '';
        }
    }
    //passwd1
    //lose focus 
    passwd1.onblur = function () {
        //Match with regular expressions 
        if (passwd1.value != "") {
            span2.style.color = 'green';
            span2.innerText = '✓ Available';
            flag_passwd1 = true;
        } else {
            span2.style.color = 'rgba(253, 40, 69, 1)';
            span2.innerText = '✖ Password can not be blank';
            flag_passwd1 = false;
        }
    }
    passwd1.onfocus = function () {
        span2.innerText = '';
    }
    //passwd2
    //lose focus       
    passwd2.onblur = function () {
        if (passwd1.value == "") {
            span3.style.color = 'rgba(253, 40, 69, 1)';
            span3.innerText = '✖ Please enter password first ';
            flag_passwd2 = false;
        }
        else if (passwd2.value == "") {
            span3.style.color = 'rgba(253, 40, 69, 1)';
            span3.innerText = '✖ Please confirm your password ';
            flag_passwd2 = false;
        }
        else if (passwd1.value == passwd2.value) {
            span3.style.color = 'green';
            span3.innerText = '✓ Available';
            flag_passwd2 = true;
        } else {
            span3.style.color = 'rgba(253, 40, 69, 1)';
            span3.innerText = '✖ Inconsistent password entered twice ';
            flag_passwd2 = false;
        }
    }
    passwd2.onfocus = function () {
        if (!flag_passwd2) {
            passwd2.value = "";
            span3.innerText = '';
        }
    }
    //email
    //lose focus 
    email.onblur = function () {
        var reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
        if (email.value == "") {
            span4.style.color = 'rgba(253, 40, 69, 1)';
            span4.innerText = '✖ E-mail can not be empty ';
            flag_email = false;
        } else
            if (reg.test(email.value)) {
                span4.style.color = 'green';
                span4.innerText = '✓ Available ';
                flag_email = true;
            } else {
                span4.style.color = 'rgba(253, 40, 69, 1)';
                span4.innerText = '✖ Email is unavailable ';
                flag_email = false;
            }
    }
    email.onfocus = function () {
        if (!flag_email) {
            email.value = "";
            span4.innerText = '';
        }
    }
    //address
    //lose focus 
    address.onblur = function () {
        if (address.value != "") {
            span5.style.color = 'green';
            span5.innerText = '✓ Available';
            flag_address = true;
        } else {
            span5.style.color = 'rgba(253, 40, 69, 1)';
            span5.innerText = '✖ Address can not be empty ';
            flag_address = false;
        }
    }

    address.onfocus = function () {
        span5.innerText = '';
    }
    //mobile
    //lose focus 
    mobile.onblur = function () {
        var reg = /^\(0([0-9]{3})\)+[0-9]{6,11}$/;
        if (reg.test(mobile.value)) {
            span6.style.color = 'green';
            span6.innerText = '✓ Available';
            flag_mobile = true;
        } else {
            span6.style.color = 'rgba(253, 40, 69, 1)';
            span6.innerText = '✖ Phone number format is \n  (country code)number eg. (0013)102030405';
            flag_mobile = false;
        }
    }
    mobile.onfocus = function () {
        span6.innerText = '';
    }
    //birthday
    //lose focus




    birthday.onblur = function () {
        var nowTime = new Date().getTime(),

            // var 
            //     strBirthday = strBirthday.split('-')
            //     birthYear = strBirthdayArr[0],
            //     birthMonth = strBirthdayArr[1],
            //     birthDay = strBirthdayArr[2],
            //     c = new Date(),
            //     nowYear = c.getfullYear(),
            //     nowMonth = c.getMonth() + 1,
            //     nowDay = c.getDate()
            //     console.log(C)
            // if(nowYear == birthYear){
            //     return = 0
            //     var abc = nowDay - birthDay

            // }
            // if(nowMonth == birthMonth){
            //     var abc = nowDay - birthDay  
            // }          
            //     var b = new Date().getFullYear();
            a = new Date(birthday.value).getTime();
        let years = Math.ceil((nowTime - a) / 31536000000);
        // console.log(Math.ceil((nowTime-a)/31536000000))
        // console.log(a)
        // nowYear = a.getFullYear(),
        // nowMonth = a.getMonth() + 1,
        // nowDay = a.getDate();

        //console.log(a)
        //console.log(years)
        if (years > 13 && years < 50) {
            span7.style.color = 'green';
            span7.innerText = '✓ Available';
            flag_birthday = true;
        } else if (years <= 13) {
            //debugger
            span7.style.color = 'red';
            span7.innerText = '✖ You are too young to regist!';
            flag_birthday = false;
        } else if(years>=50){
            var r = confirm("Do you need customer service help? ");
            if (r == true) {
                window.location.href = "help.html";
                span7.style.color = 'green';
                span7.innerText = '✓ Available';
                flag_birthday = true;
            }
            else {
            span7.style.color = 'green';
            span7.innerText = '✓ Available';
            flag_birthday = true;
            }
        }
    }

    birthday.onfocus = function () {
        span7.innerText = '';
    }
}

//var x=document.getElementById("location");
//function getLocation()
//{
//if (navigator.geolocation)
//  {
// navigator.geolocation.getCurrentPosition(showPosition);
//  }
//else{x.innerHTML="Geolocation is not supported by this browser.";}
//}
//function showPosition(position)
//{
//x.innerHTML="Latitude: " + position.coords.latitude +
//"<br />Longitude: " + position.coords.longitude;
//}
//